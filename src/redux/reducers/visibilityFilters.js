import * as constants from '../actions/constants';

export const visibilityFilters = (state = constants.VisibilityFilters.SHOW_ALL, action) => {
    switch(action.type) {
        case constants.SET_VISIBILITY_FILTER:
            return action.filter;
        default:
            return state;
    }
};