import React, {Component} from 'react';

import ConnectTodoList from './containers/ConnectTodoList';
import AddTodo from './components/AddTodo';

class App extends Component {
    render() {
        return (
            <div>
                <AddTodo />
                <ConnectTodoList />
            </div>
        );
    }
}

export default App;
