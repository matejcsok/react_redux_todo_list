import {VisibilityFilters} from "../redux/actions/constants";
import TodoList from '../components/TodoList';
import { connect } from 'react-redux'
import { toggleTodo } from '../redux/actions/index';

const getVisivleTodoList = (todos, filter) => {
    switch(filter) {
        case VisibilityFilters.SHOW_ACTIVE:
            return todos.filter(todo => !todo.completed);
        case VisibilityFilters.SHOW_COMPLETED:
            return todos.filter(todo => todo.completed);
        case VisibilityFilters.SHOW_ALL:
        default:
            return todos;
    }
};

const mapStateToProps = state => ({
    todos: getVisivleTodoList(state.todos, state.visibilityFilters),
});

const mapDispatchToProps = dispatch => ({
    toggleTodo: id => dispatch(toggleTodo(id)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TodoList)