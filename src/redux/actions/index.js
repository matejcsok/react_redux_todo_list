import * as constants from './constants';

let id = 0;

export const addTodo = text => ({
    type: constants.ADD_TODO,
    id: id++,
    text,
});

export const toggleTodo = id => ({
   type: constants.TOGGLE_TODO,
   id,
});

export const setVisibilityFilters = filter => ({
   type: constants.SET_VISIBILITY_FILTER,
   filter,
});